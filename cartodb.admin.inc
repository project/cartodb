<?php

/**
 * @file
 * Administration file for CartoDB module.
 */

/**
 * CartoDB admin settings form.
 */
function cartodb_admin_settings_form($form, &$form_state) {
  $form['cartodb_username'] = array(
    '#title' => t('Username'),
    '#type' => 'textfield',
    '#default_value' => variable_get('cartodb_username', 'username'),
    '#description' => t('CartoDB username/subdomain. I.e. username.cartodb.com'),
    '#required' => TRUE,
  );
  $form['cartodb_password'] = array(
    '#title' => t('Password'),
    '#type' => 'textfield',
    '#default_value' => variable_get('cartodb_password', 'password'),
    '#required' => TRUE,
  );
  $form['cartodb_email'] = array(
    '#title' => t('Email'),
    '#type' => 'textfield',
    '#default_value' => variable_get('cartodb_email', 'account@domain.com'),
    '#required' => TRUE,
  );
  $form['cartodb_consumer_key'] = array(
    '#title' => t('Consumer key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('cartodb_consumer_key', 'key'),
    '#description' => t('CartoDB public key provided.'),
    '#required' => TRUE,
  );
  $form['cartodb_consumer_secret'] = array(
    '#title' => t('Consumer secret'),
    '#type' => 'textfield',
    '#default_value' => variable_get('cartodb_consumer_secret', 'secret'),
    '#description' => t('CartoDB private key provided.'),
    '#required' => TRUE,
  );

  $form = system_settings_form($form);

  $form['actions']['check'] = array(
    '#value' => t('Check configuration'),
    '#type' => 'submit',
    '#submit' => array('cartodb_account_form_check_submit'),
  );

  return $form;
}

function cartodb_account_form_validate(&$form, &$form_state) {
  if (!valid_email_address($form_state['values']['cartodb_email'])) {
    form_set_error('cartodb_email', t('Invalid email address'));
  }
}

function cartodb_account_form_check_submit(&$form, &$form_state) {
  $client = cartodb_client();
  if ($client->authorized) {
    drupal_set_message(t('Connection to CartoDB stablished. You configuration is correct.'));
  }
  else {
    drupal_set_message(t('Impossible to connect to CartoDB. Check your configuration or your network status.'), 'error');
  }
}
