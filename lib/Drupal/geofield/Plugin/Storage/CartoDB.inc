<?php

/**
 * @file
 * Definition of DrupalGeofieldPluginStorageCartoDB.
 */

// @todo - Uncomment in D8
//namespace Drupal\geofield\Plugin\Storage;

class DrupalGeofieldPluginStorageCartoDB extends DrupalGeofieldPluginStorageRemote implements DrupalGeofieldPluginStorageRemoteInterface {
  protected $config;
  public $client;

  function __construct() {
    require_once libraries_get_path('cartodbclient-php') . '/cartodb.class.php';
    $this->config = array(
      'subdomain' => variable_get('cartodb_username', 'username'),
      'password' => variable_get('cartodb_password', 'password'),
      'email' => variable_get('cartodb_email', 'account@domain.com'),
      'key' => variable_get('cartodb_consumer_key', 'key'),
      'secret' => variable_get('cartodb_consumer_secret', 'secret'),
    );
    $this->client = new CartoDBClient($this->config);
  }

  function getTableName($field_name) {
    return 'dr_' . $field_name;
  }

  public function createTable($name) {
    if (!$this->client->authorized) {
      drupal_set_message(t('CartoDB Error - Couldn\'t create the field. Check your configuration or your network status.'), 'error');
      return NULL;
    }
    $response = $this->client->createTable($name);
    if ($response['info']['http_code'] != 200) {
      $error_message = t('CartoDB Error - Couldn\'t create the table. Check your configuration or your network status.');
      $error_message .= $this->builErrorMessage($response);
      drupal_set_message($error_message, 'error');
      return NULL;
    }
    return $response['return']['name'];
  }

  public function deleteTable($name) {
    if (!$this->client->authorized) {
      drupal_set_message(t('CartoDB Error - Couldn\'t delete the field. Check your configuration or your network status.'));
      return NULL;
    }
    try {
      $response = $this->client->dropTable($name);
      if ($response['info']['http_code'] != 200) {
        $error_message = t('CartoDB Error - Couldn\'t delete the table. Check your configuration or your network status');
        $error_message .= $this->builErrorMessage($response);
        drupal_set_message($error_message, 'error');
        return NULL;
      }
    }
    catch (Exception $e) {
      drupal_set_message(t('CartoDB API failed: @message', array('@message' => $e->getMessage())), 'warning');
    }
    return 0;
  }

  public function insertRow($table, $wkt) {
    try {
      $return = $this->client->insertRow($table, array('the_geom' => "ST_GeomFromText('" . $wkt . "', 4326)"));
      if ($return['info']['http_code'] == 200) {
        return $return['return']['rows'][0]->cartodb_id;
      }
    }
    catch (Exception $e) {
      drupal_set_message(t('CartoDB API failed: @message', array('@message' => $e->getMessage())), 'warning');
    }
    return NULL;
  }

  public function deleteRow($table, $id) {
    $this->client->deleteRow($table, $id);
  }

  public function updateRow($table, $id, $wkt) {
    $return = $this->client->updateRow($table, $id, array('the_geom' => "ST_GeomFromText('" . $wkt . "', 4326)"));
    if ($return['info']['http_code'] == 200) {
      return 0;
    }
    return NULL;
  }

  protected function builErrorMessage($response) {
    return t('Error HTTP code: %http_code, CartoDB error: %code - %description',
      array(
        '%http_code' => $response['info']['http_code'],
        '%code' => $response['return']['code'],
        '%description' => $response['return']['code'],
      )
    );
  }
}
